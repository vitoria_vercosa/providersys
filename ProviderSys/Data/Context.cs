﻿using Microsoft.EntityFrameworkCore;
using ProviderSys.Models;

namespace ProviderSys.Data
{
    public class Context : DbContext
    {
        public DbSet<Product> Product { get; set; }

        public Context(DbContextOptions<Context> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(x => x.ProductCode);
        }
    }
}
