﻿CREATE TABLE [dbo].[Product]
(
	[ProductCode] INT NOT NULL IDENTITY PRIMARY KEY, 
    [ProductName] NVARCHAR(50) NOT NULL, 
    [ProductDescription] NVARCHAR(MAX) NULL, 
    [Active] BIT NOT NULL, 
    [ManufacturingDate] DATE NULL, 
    [ExpirationDate] DATE NULL, 
    [Price] FLOAT NULL, 
    [ProviderCode] INT NULL, 
    [ProviderDescription] NVARCHAR(MAX) NULL, 
    [ProviderLEI] NVARCHAR(20) NULL

)