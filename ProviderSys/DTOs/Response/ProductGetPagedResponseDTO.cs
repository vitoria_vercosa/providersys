﻿using System.Collections.Generic;

namespace ProviderSys.DTOs.Response
{
    public class ProductGetPagedResponseDTO
    {
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<ProductGetResponseDTO> Data { get; set; }

        public ProductGetPagedResponseDTO()
        {

        }

        public ProductGetPagedResponseDTO(int totalItems, int totalPages, int pageNumber, int pageSize, IEnumerable<ProductGetResponseDTO> data)
        {
            this.TotalItems = totalItems;
            this.TotalPages = totalPages;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Data = data;
        }
    }
}
