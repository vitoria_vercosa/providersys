﻿using System;

namespace ProviderSys.DTOs.Response
{
    public class ProductGetResponseDTO
    {
        public int ProductCode { get; set; }
        public string ProductName { get; set; }
        public string? ProductDescription { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public double? Price { get; set; }
        public int? ProviderCode { get; set; }
        public string? ProviderDescription { get; set; }

        public ProductGetResponseDTO()
        {

        }
        public ProductGetResponseDTO(
            int productCode, 
            string productName, 
            string productDescription, 
            DateTime? manufacturingDate, 
            DateTime? expirationDate,
            double? price,
            int? providerCode,
            string? providerDescription)
        {
            this.ProductCode = productCode;
            this.ProductName = productName;
            this.ProductDescription = productDescription;
            this.ManufacturingDate = manufacturingDate;
            this.ExpirationDate = expirationDate;
            this.Price = price;
            this.ProviderCode = providerCode;
            this.ProviderDescription = providerDescription;
        }

    }
}
