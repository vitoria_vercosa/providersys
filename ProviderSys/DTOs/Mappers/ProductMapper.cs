﻿using AutoMapper;
using ProviderSys.DTOs.Request;
using ProviderSys.DTOs.Response;
using ProviderSys.Models;

namespace ProviderSys.DTOs.Mappers
{
    public class ProductMapper : Profile
    {
        public ProductMapper()
        {
            CreateMap<ProductCreateRequestDTO, Product>();
            CreateMap<ProductUpdateRequestDTO, Product>();
            CreateMap<Product, ProductGetResponseDTO>();
        }
    }
}
