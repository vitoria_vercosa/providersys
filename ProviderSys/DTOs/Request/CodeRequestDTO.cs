﻿namespace ProviderSys.DTOs.Request
{
    public class CodeRequestDTO
    {
        public int Code { get; set; }
    }
}
