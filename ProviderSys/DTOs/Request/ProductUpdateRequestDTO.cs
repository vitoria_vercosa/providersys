﻿using System;

namespace ProviderSys.DTOs.Request
{
    public class ProductUpdateRequestDTO
    {
        public int ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public DateTime ManufacturingDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public double Price { get; set; }
        public int? ProviderCode { get; set; }
        public string ProviderDescription { get; set; }
        public string ProviderLEI { get; set; }
    }
}
