﻿using FluentValidation;

namespace ProviderSys.DTOs.Request.Validators
{
    public class CodeRequestDTOValidator : AbstractValidator<CodeRequestDTO>
    {
        public CodeRequestDTOValidator()
        {
            RuleFor(x => x.Code)
                .NotNull()
                .NotEmpty()
                .WithMessage("The 'Code' cannot be null or empty.")
                .GreaterThan(0)
                .WithMessage("The 'Code' is invalid.");
        }
    }
}
