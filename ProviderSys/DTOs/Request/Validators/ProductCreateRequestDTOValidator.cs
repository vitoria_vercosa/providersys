﻿using FluentValidation;
using System;

namespace ProviderSys.DTOs.Request.Validators
{
    public class ProductCreateRequestDTOValidator : AbstractValidator<ProductCreateRequestDTO>
    {
        public ProductCreateRequestDTOValidator()
        {

            RuleFor(x => x.ProductName)
                .NotNull()
                .NotEmpty()
                .WithMessage("The 'Product Name' cannot be null or empty.");

            RuleFor(x => x.ManufacturingDate)
                .NotNull()
                .NotEmpty()
                .WithMessage("The 'Manufacturing Date' cannot be null or empty.")
                .LessThanOrEqualTo(DateTime.Now.AddMinutes(5))
                .WithMessage("The 'Manufacturing Date' is invalid - It cannot be greater than the Current Date.");

            RuleFor(x => x.ExpirationDate)
                .NotEmpty()
                .NotEmpty()
                .WithMessage("The 'Expiration Date' cannot be null or empty.")
                .GreaterThanOrEqualTo(x => x.ManufacturingDate)
                .WithMessage("The 'Expiration Date' is invalid - It cannot be less than or equal to the 'Manufacturing Date'.");

            RuleFor(x => x.Price)
                .NotEmpty()
                .NotNull()
                .WithMessage("The 'Price' cannot be null or empty.")
                .GreaterThanOrEqualTo(0)
                .WithMessage("The 'Price' is invalid - It cannot be less or equal to 0.");

            RuleFor(x => x.ProviderCode)
                .NotEmpty()
                .NotNull()
                .WithMessage("The 'Provider Code' cannot be null or empty.")
                .GreaterThanOrEqualTo(0)
                .WithMessage("The 'Provider Code' is invalid - It cannot be less or equal to 0.");

            RuleFor(x => x.ProviderLEI)
                .NotNull()
                .NotEmpty()
                .WithMessage("The 'Provider LEI' cannot be null or empty.");

        }
    }
}
