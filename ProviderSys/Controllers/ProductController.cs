﻿using Microsoft.AspNetCore.Mvc;
using ProviderSys.DTOs.Request;
using ProviderSys.Enums;
using ProviderSys.Services.Interfaces;
using System.Threading.Tasks;

namespace ProviderSys.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {

        //private readonly Context _context;
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByCode(int code)
        {
            var result = await _productService.GetByCode(code);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductCreateRequestDTO request)
        {
            var result = await _productService.Create(request);
            return Created("", result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProduct(ProductUpdateRequestDTO request)
        {
            var result = await _productService.Update(request);
            return NoContent();

        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int code)
        {
            await _productService.Delete(code);
            return NoContent();
        }

        [HttpGet("get-paged")]
        public async Task<IActionResult> GetPaged(string search, OrderByPropertyEnum orderBy, OrderTypeEnum orderType, int pageNumber, int pageSize)
        {
            return Ok(await _productService.GetPaged(search, orderBy.ToString(), orderType.ToString(), pageNumber, pageSize));
        }
    }
}
