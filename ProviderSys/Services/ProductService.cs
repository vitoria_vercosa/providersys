﻿using AutoMapper;
using ProviderSys.Configs;
using ProviderSys.DTOs.Request;
using ProviderSys.DTOs.Request.Validators;
using ProviderSys.DTOs.Response;
using ProviderSys.Exceptions;
using ProviderSys.Models;
using ProviderSys.Repositories.Interfaces;
using ProviderSys.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;

namespace ProviderSys.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IValidationHelper _validation;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository productRepository, IValidationHelper validation, IMapper mapper)
        {
            _productRepository = productRepository;
            _validation = validation;
            _mapper = mapper;
        }

        public async Task<ProductGetResponseDTO> GetByCode(int code)
        {
            await _validation.ValidateAsync<CodeRequestDTOValidator, CodeRequestDTO>(new CodeRequestDTO { Code=code});

            var product = await _productRepository.GetByCode(code);

            if (product == null) throw new NotFoundException("There is no Product with this code.");

            return _mapper.Map<ProductGetResponseDTO>(product);
        }

        public async Task<int> Create(ProductCreateRequestDTO request)
        {
            await _validation.ValidateAsync<ProductCreateRequestDTOValidator, ProductCreateRequestDTO>(request);

            if (await _productRepository.ExistsByName(request.ProductName)) 
                throw new BadRequestException($"Already exists a product with this name: {request.ProductName}.");

            var product = _mapper.Map<Product>(request);
            product.Active = true;

            await _productRepository.Add(product);

            return product.ProductCode;
        }

        public async Task<bool> Update(ProductUpdateRequestDTO request)
        {
            await _validation.ValidateAsync<ProductUpdateRequestDTOValidator, ProductUpdateRequestDTO>(request);

            var productToUpdate = await _productRepository.GetByCode(request.ProductCode);

            if (productToUpdate == null)
                throw new NotFoundException("There is no Product with this code.");
            /*
            productToUpdate.ProductName = request.ProductName;
            productToUpdate.ProviderDescription = request.ProviderDescription;
            productToUpdate.ManufacturingDate = request.ManufacturingDate;
            productToUpdate.ExpirationDate = request.ExpirationDate;
            productToUpdate.Price = request.Price;
            productToUpdate.ProviderCode = request.ProviderCode;
            productToUpdate.ProviderDescription = request.ProviderDescription;
            productToUpdate.ProviderLEI = request.ProviderLEI;
            */

            _mapper.Map<ProductUpdateRequestDTO, Product>(request, productToUpdate);

            await _productRepository.Update(productToUpdate);
            return true;
        }

        public async Task<bool> Delete(int code)
        {
            await _validation.ValidateAsync<CodeRequestDTOValidator, CodeRequestDTO>(new CodeRequestDTO { Code = code });

            var product = await _productRepository.GetByCode(code);

            if (product == null) throw new NotFoundException("There is no Product with this code.");

            product.Active = false;

            await _productRepository.Update(product);
            return true;
        }

        public async Task<ProductGetPagedResponseDTO> GetPaged(string search, string orderBy, string orderType, int pageNumber, int pageSize)
        {
            if (pageNumber <= 0 || pageSize <= 0)
                throw new BadRequestException("The pagination request cannot be realized with invalid input.");

            var data = await _productRepository.GetPaged(search,orderBy, orderType, pageNumber, pageSize);

            List<ProductGetResponseDTO> rows = new List<ProductGetResponseDTO> { };

            var totalRows = data[0].TOTALROWS;

            foreach (var item in data)
            {
                ProductGetResponseDTO row = new ProductGetResponseDTO(
                                                    item.ProductCode,
                                                    item.ProductName,
                                                    item.ProductDescription,
                                                    item.ManufacturingDate,
                                                    item.ExpirationDate,
                                                    item.Price,
                                                    item.ProviderCode,
                                                    item.ProviderDescription
                                                );
                rows.Add( row );
            }

            ProductGetPagedResponseDTO response = new ProductGetPagedResponseDTO(
                data[0].TOTALROWS,
                (int)Math.Ceiling((double)data[0].TOTALROWS / pageSize),
                pageNumber,
                pageSize,
                rows
                );

            return response;

        }
    }
}
