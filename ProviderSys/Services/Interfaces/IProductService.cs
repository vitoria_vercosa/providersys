﻿using ProviderSys.DTOs.Request;
using ProviderSys.DTOs.Response;
using ProviderSys.Models;
using System.Threading.Tasks;

namespace ProviderSys.Services.Interfaces
{
    public interface IProductService
    {
        Task<ProductGetResponseDTO> GetByCode(int code);
        Task<int> Create(ProductCreateRequestDTO request);
        Task<bool> Update(ProductUpdateRequestDTO request);
        Task<bool> Delete(int code);
        Task<ProductGetPagedResponseDTO> GetPaged(string search, string orderBy, string orderType, int pageNumber, int pageSize);
    }
}
