﻿using System.Net;
using System;

namespace ProviderSys.Exceptions
{
    public class NotFoundException : Exception
    {
        public HttpStatusCode StatusCode { get; private set; }
        public NotFoundException(string message) : base(message)
        {
            StatusCode = HttpStatusCode.NotFound;
        }

    }
}
