﻿using System;

namespace ProviderSys.Models
{
    public class Product
    {
        public int ProductCode { get; set; }
        public string ProductName { get; set; }
        public string? ProductDescription { get; set; }
        public bool Active { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public double? Price { get; set; }
        public int? ProviderCode { get; set; }
        public string? ProviderDescription { get; set; }
        // Legal Entity Identifier -> CNPJ
        public string? ProviderLEI { get; set; }

        public Product()
        {

        }
        public Product(
            string productName, 
            string? productDescription, 
            bool active,
            DateTime? manufacturingDate,
            DateTime? expirationDate,
            double? price,
            int? providerCode,
            string? providerDescription,
            string? providerLEI
            )
        {
            ProductName = productName;
            ProductDescription = productDescription;
            Active = active;
            ManufacturingDate = manufacturingDate;
            ExpirationDate = expirationDate;
            Price = price;
            ProviderCode = providerCode;
            ProviderDescription = providerDescription;
            ProviderLEI = providerLEI;
        }

    }
}
