﻿using ProviderSys.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProviderSys.Repositories.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAll();
        Task<Product?> GetByCode(int code);
        Task<double?> GetPrice(int code);
        Task Add(Product product);
        Task Update(Product product);
        Task<dynamic> GetPaged(string search, string orderBy, string orderType, int pageNumber, int pageSize);

        Task<bool> ExistsByName(string name);
        Task<Product> GetByName(string name);
    }
}
