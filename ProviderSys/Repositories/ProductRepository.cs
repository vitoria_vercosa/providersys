﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using ProviderSys.Data;
using ProviderSys.Models;
using ProviderSys.Repositories.Interfaces;
using ProviderSys.Repositories.Scripts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProviderSys.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly Context _context;

        public ProductRepository(Context context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            return await _context.Product.ToListAsync();
        }

        public async Task<Product?> GetByCode(int code)
        {
            try
            {
                return await _context.Product.FirstOrDefaultAsync(x => x.ProductCode == code && x.Active == true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<double?> GetPrice(int code)
        {
            try
            {
                var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductCode == code);
                return product.Price;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task Add(Product product)
        {
            try
            {
                await _context.Product.AddAsync(product);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task Update(Product product)
        {
            try
            {
                _context.Product.Update(product);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> ExistsByName(string name)
        {
            try
            {
                return await _context.Product.AnyAsync(x => x.ProductName == name);
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Product> GetByName(string name)
        {
            try
            {
                return await _context.Product.FirstOrDefaultAsync(x => x.ProductName == name);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<dynamic> GetPaged(string search, string orderBy, string orderType, int pageNumber, int pageSize)
        {
            try
            {
                string sql = ProductScripts.GETPAGED(search, orderBy, orderType, pageNumber, pageSize);
                var connection = _context.Database.GetDbConnection();

                dynamic result = await connection.QueryAsync(sql);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
