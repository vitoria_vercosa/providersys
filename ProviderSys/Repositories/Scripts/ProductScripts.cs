﻿using static System.Net.Mime.MediaTypeNames;

namespace ProviderSys.Repositories.Scripts
{
    public class ProductScripts
    {
        public static string GETPAGED(string search, string orderBy, string orderType, int pageNumber, int pageSize)
        {
            var offset = pageSize * (pageNumber - 1);
            var sql = @$"SELECT COUNT(*) OVER() TOTALROWS, 
                                          P.ProductCode, P.ProductName, P.ProductDescription, P.ManufacturingDate,
                                          P.ExpirationDate, P.Price, P.ProviderCode, P.ProviderDescription
                                          FROM Product P
                                          WHERE (UPPER(P.ProductName) LIKE UPPER('%{search}%') ) OR
                                                (UPPER(P.ProductDescription) LIKE UPPER('%{search}%') ) OR
                                                (P.Price LIKE '%{search}%' ) OR
                                                (UPPER(P.ProviderDescription) LIKE UPPER('%{search}%') )
                                          GROUP BY P.ProductCode, P.ProductName, P.ProductDescription, P.ManufacturingDate,
                                          P.ExpirationDate, P.Price, P.ProviderCode, P.ProviderDescription ";

            if (orderBy != null && orderType != null)
                sql += $@"ORDER BY {orderBy} {orderType} ";

            sql += $@"OFFSET {offset} ROWS FETCH NEXT {pageSize} ROWS ONLY";

            return sql;
        }

        // ORDER BY {orderBy} ASC
        // "
    }
}
