﻿namespace ProviderSys.Enums
{
    public enum OrderByPropertyEnum
    {
        ProductName,
        ProductDescription,
        ManufacturingDate,
        ExpirationDate,
        Price,
        ProviderDescription
    }
}
