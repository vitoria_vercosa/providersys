using Microsoft.AspNetCore.Mvc;
using Moq;
using ProviderSys.Controllers;
using ProviderSys.DTOs.Response;
using ProviderSys.Exceptions;
using ProviderSys.Services.Interfaces;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ProviderSys.UnitTest
{
    public class ProductUnitTest
    {
        private readonly ProductController _productController;
        private readonly Mock<IProductService> _mockProductService;

        public ProductUnitTest()
        {
            _mockProductService = new Mock<IProductService>();
            _productController = new ProductController(_mockProductService.Object);
        }

        [Theory(DisplayName = "GetByCode Should Return 200 Ok")]
        [InlineData(2)]
        public async void GetByCode_ShouldReturn200Ok(int code)
        {
            // Arrange
            ProductGetResponseDTO mockResult = new ProductGetResponseDTO();
            mockResult.ProductName = "some product";

            // Act
            _mockProductService.Setup(x => x.GetByCode(code)).Returns(Task.FromResult(mockResult)).Verifiable();
            var response = await _productController.GetByCode(code);
            var statusCode = (response as ObjectResult).StatusCode;

            // Assert
            Assert.Equal(200, statusCode);
        }
        

    }
}
